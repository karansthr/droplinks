import asyncio

import motor.motor_asyncio
import uvloop

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
client = motor.motor_asyncio.AsyncIOMotorClient()
database = client.droplinks
